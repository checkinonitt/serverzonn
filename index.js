const express = require('express');
const app = express()
const server = require('http').Server(app)
const serverWeb = require('http').Server(app)

const process = require("process")
const dbWrapper = require('./utilities/db_wrapper')
const vhost = require('vhost')
const broadcastOpcode = require('./utilities/constants').constants.broadcastOpcode

process.on("uncaughtException", (e) => {
	console.error(e.message, e.stack)
})

const ioWeb = require('socket.io')(serverWeb)
const socketApi = require('./socket_api').init(server)
const clockRest = require('./web_api')(app)


// DEFINES

const UUID = 0
const MAJOR = 1
const MINOR = 2
const TIME = 3
const PORT = 5555
const WEB_PORT = 5001
// const PRESENTEST = 10000

const VALID_UUID = "20CAE8A0-A9CF-11E3-A5E2-0800200C9A66";

server.listen(PORT, function () {
	console.log(`Clock app listen on port ${PORT}!`);
});

serverWeb.listen(WEB_PORT, (e) => {
	console.log("Web app is on");
});