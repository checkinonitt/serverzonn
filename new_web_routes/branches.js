const dbWrapper = require('../utilities/db_wrapper')

let getBranchesNames = (req, res) => {
    dbWrapper.getBranchesNames((err, branches) => {
        if (err) res.status(401).send("cannot get branches names")
        else res.send(branches)
    })
}

module.exports = function(app) {
    app.post('/api/branches', getBranchesNames)
}