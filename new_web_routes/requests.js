const dbWrapper = require('../utilities/db_wrapper')

let getRequests = (req, res) => {
    let data = req.body.data
    let gigaMajor = data.gigaMajor
    let gigaMinor = data.gigaMinor

    dbWrapper.getRequests(gigaMajor, gigaMinor, (err, requests) => {
        if (err) res.status(401).send("cannot get requests")
        else res.send(requests)
    })
}

let approveRequest = (req, res) => {
    requestDecision(req, res, true)
}

let rejectRequest = (req, res) => {
    requestDecision(req, res, false)
}

let requestDecision = (req, res, isApproved) => {
    let request = req.body.data.request
    let username = request.username
    let companyId =  request.companyId
    let branchId =  request.branchId
    let branchPhoneNumber = request.branchPhoneNumber
    let gigaId = request.gigaId
    let messageId = request.messageId
    let decisionDescription = request.decisionDescription

    dbWrapper.getGigasInfo([
        {
            major: parseInt(companyId),
            minor: parseInt(branchId * 256) + parseInt(gigaId)
        }
    ], (gigasInfo) => {
        dbWrapper.requestDecision(username, companyId, branchId, branchPhoneNumber,
                                  gigaId, gigasInfo[0].gigaName, messageId, 
                                  decisionDescription, isApproved, (err, status) => {

            if (err) res.status(401).send("cannot change request status")
            else res.send(status)
        })
    })
}

let uncheckRequest = (req, res) => {
    let data = req.body.data
    let companyId = data.request.companyId
    let branchId = data.request.branchId
    let gigaId = data.request.gigaId
    let messageId = data.request.messageId

    dbWrapper.uncheckRequest(companyId, branchId, gigaId, messageId, (err, status) => {
        if (err) res.status(401).send("cannot change request status")
        else res.send(status)
    })
}

module.exports = function(app) {
    app.post('/api/requests', getRequests)
    app.post('/api/requests/approve', approveRequest)
    app.post('/api/requests/reject', rejectRequest)
    app.post('/api/requests/uncheck', uncheckRequest)
}