var firebase = require("firebase-admin");
var uuid = require('node-uuid');
var firebase_account = require("./demogiga-8c9c3-firebase-adminsdk-pqdv4-0be3d726e3.json");
var crypto = require("crypto")

firebase.initializeApp({
  credential: firebase.credential.cert(firebase_account),
  databaseURL: "https://demogiga-8c9c3.firebaseio.com"
});

function hash_pass(password) {
	return crypto.createHash('sha256').update(password).digest('hex');
}

function set_test_user(username, id, secret, time, password) {
  firebase.database().ref('users/' + username).set({
    id: id,
	secret: secret,
	time: time,
	password: hash_pass(password)
  });
  
  firebase.database().ref('users/' + username + "/contacts/1").set({
	  phone: "123",
  });
  
  firebase.database().ref('users/' + username + "/contacts/2").set({
	  phone: "456",
  });
  
  firebase.database().ref('users/' + username + "/contacts/3").set({
	  phone: "789",
  });
  
  firebase.database().ref('users/' + username + "/table/1").set({
	  date: "1/1/1",
	  time: "00:00"
  });
  
  firebase.database().ref('users/' + username + "/table/2").set({
	  date: "1/2/1",
	  time: "00:22"
  });
  
  firebase.database().ref('users/' + username + "/table/3").set({
	  date: "1/1/112",
	  time: "33:00"
  });
  
  
}



//set_test_user("idan", 3, "khaled", "6 hours", "123456");
//set_test_user("dj khaled", 3, "khaled", "6 hours", "anotherone");
var key = 123;

var new_object = new Object();
				new_object[key] = new Object();
					new_object[key].lastseen = 1;
					new_object[key].start = 2;
					new_object[key].lastseen = 3;
console.log(new_object);