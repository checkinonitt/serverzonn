var io, dbWrapper
var fcmTokenByUsername = {'yossi': 'default'}

module.exports = {
    init: function (server) {
        io = require('socket.io')(server)
        dbWrapper = require('./utilities/db_wrapper')

        io.on('connection', function (socket) {
            socket.username = "yossi"
            socket.age = 27
            socket.phoneNumber = '0503234533'
            fcmTokenByUsername[socket.username] = 'default'
            
            socket.on("gigasInRange", function (gigasNearBy) {
                gigasNearBy = JSON.parse(gigasNearBy)
                
                if(gigasNearBy.length) {
                    dbWrapper.getGigasInfo(gigasNearBy, (gigasInfo) => {
                        socket.emit("gigasInRangeResponse", JSON.stringify(gigasInfo))
                    })
                } else {
                    socket.emit("gigasInRangeResponse", "[]")
                }
            })

            socket.on("uploadRequest", function (data) {
                data = JSON.parse(data)
                
                if(data.length) {
                    let fcmToken = data[0].fcmToken
                    fcmTokenByUsername[socket.username] = fcmToken
                    
                    let username = socket.username
                    let gigaMajor = data[0].major
                    let gigaMinor = data[0].minor
                    let title = data[0].title
                    let content = data[0].content

                    dbWrapper.uploadRequest(username, gigaMajor, gigaMinor, title, content, (ans) => {

                        socket.emit("uploadRequest", ans)
                    })
                }
            })
        })
    },
    fcmTokenByUsername: fcmTokenByUsername
}