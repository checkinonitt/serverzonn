var noActivityTreshold = require('./constants').constants.noActivityTreshold

exports.isAtWork = (lastShift) => {
    if (lastShift == undefined) return false
    // If android - check difference between last seen to now compared to no activity treshold
    // If ios     - check if shift time is 0, means 'out' event hasn't been triggered yet
    if (lastShift.os == 'android') {
        if (noActivityTreshold >= ((new Date()).getTime() - lastShift.lastseen)) {
            return true
        }
    } else if (lastShift.os = 'ios') {
        if (lastShift.time == 0) {
            return true
        }
    }

    return false
}