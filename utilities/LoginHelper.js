const jwt = require('jsonwebtoken')
const crypto = require('crypto')

module.exports = {
    createToken: (id, secret) => {
        var json = {
            id: id
        }

        var token = jwt.sign(json, secret, {
            expiresIn: "30d"
        });

        return token;
    },

    hashText: (textToHash) => {
        return crypto.createHash('sha256').update(textToHash).digest('hex')
    }
}