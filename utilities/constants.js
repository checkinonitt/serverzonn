const constants = {
    noActivityTreshold: 60 * 1000, // minute in ms
    forecastDaysNumber: 3,
    defaultPassword: "passpass",
    weatherAPIkey: "78bd6af01c8219ab51669ab244a80834", // openweathermap
    maxTimeToNewAndroidShift: 600000, // 10 minutes in ms
    broadcastOpcode: 0,
    messageOpcode: 1
};

exports.constants = constants