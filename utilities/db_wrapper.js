const requestsQueries = require('./mongoQueries/requests')
const branchesQueries = require('./mongoQueries/branches')
const gigasQueries = require('./mongoQueries/gigas')

module.exports = {
    // REQUESTS
    'getRequests': requestsQueries.getRequests,
    'requestDecision': requestsQueries.requestDecision,
    'uncheckRequest': requestsQueries.uncheckRequest,
    'uploadRequest': requestsQueries.uploadRequest,
    // BRANCHES
    'getBranchesNames': branchesQueries.getBranchesNames,
    // GIGAS
    'getGigasInfo': gigasQueries.getGigasInfo
}