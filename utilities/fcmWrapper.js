var FCM = require('fcm-node')
// TODO: remove server key from here...
var serverkey = 'AAAAFmR_0cA:APA91bFqJwmcgd2lQPKqQavcwHSCwIup6rY_zMFLYxWwsgkTbnmPHbZhXQde7XeRjTmw_SD1pcaiYWtp3hfXQxzNTp53xGtlTvc8MS7QA-oxfzhsIdoeS_BMPMG-TiECljQAJWsVEvI9'
var fcm = new FCM(serverkey)
const _ = require("lodash")

let notifyRequestAnswer = (clientFCMToken, gigaName, branchPhoneNumber, content, isApproved, callback) => {
    var message = {
        to: clientFCMToken, 
        data: {
            'zonnName': gigaName,
            'zonnPhoneNumber': branchPhoneNumber,
            'isApproved': isApproved,
            'content': content
        },
        notification: {
            title: `${ isApproved ? 'קיבלנו את בקשתך' : 'דחינו את בקשתך'}`, 
            body: content,
            priority : "high"
        }
    }
    
    fcm.send(message, callback)
}

module.exports = {
    'notifyRequestAnswer': notifyRequestAnswer
}