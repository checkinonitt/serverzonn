const getDb = require("../mongo_wrapper").getDb

var getBranchesNames = (callback) => {
    getDb((db) => {
        db.collection("companies").find().toArray((err, companies) => {
            console.log(companies)
            let retBranches = []

            companies.forEach((company) => {
                company.branches.forEach((branch) => {
                    retBranches.push({
                        'companyId': company.id,
                        'companyName': company.name,
                        'branchId': branch.id,
                        'branchName': branch.name,
                        'phoneNumber': branch.phoneNumber
                    })
                })
            })

            if (err) callback(err, retBranches)
            else callback(err, retBranches)
        })
    })
}

module.exports = {
    "getBranchesNames": getBranchesNames
}