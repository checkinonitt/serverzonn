const getDb = require("../mongo_wrapper").getDb

var getGigasInfo = (gigasNearBy, callback) => {
    if (gigasNearBy && gigasNearBy.length) {
        getDb((db) => {
            db.collection("companies").find().toArray((err, companies) => {
                if (err) { 
                    callback(null)
                    return
                }

                let gigasInfo = []

                if (companies && companies.length) {
                    companies.forEach((company) => {
                        if (company && company.branches.length) {
                            company.branches.forEach((branch) => {
                                if (branch.gigas && branch.gigas.length) {
                                    branch.gigas.forEach((giga) => {
                                        gigasNearBy.forEach(nearByGiga => {
                                            // check if giga matches comanyId, branchId, gigaId
                                            if (nearByGiga.major == company.id && 
                                                parseInt(nearByGiga.minor / 256) == branch.id && 
                                                nearByGiga.minor % 256 == giga.id) {
                                                gigasInfo.push({
                                                    major: nearByGiga.major,
                                                    minor: nearByGiga.minor,
                                                    rssi: nearByGiga.rssi,
                                                    gigaName: giga.name,
                                                    gigaImagePath: giga.imagePath,
                                                    branchName: branch.name,
                                                    zonnPhoneNumber: branch.phoneNumber
                                                })
                                            }
                                        })
                                    })
                                }
                            })
                        }
                    })

                    callback(gigasInfo)
                }
            })
        })
    }
}

module.exports = {
    "getGigasInfo": getGigasInfo
}