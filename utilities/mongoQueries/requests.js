const getDb = require("../mongo_wrapper").getDb
const _ = require("lodash")
const crypto = require("crypto")
const fcmWrapper = require('../fcmWrapper')
const fcmTokenByUsername = require('../../socket_api').fcmTokenByUsername

var getRequests = (gigaMajor, gigaMinor, callback) => {
    let messagesQuery = {
        "companyId": parseInt(gigaMajor),
        "branchId": parseInt(gigaMinor)
    }

    getDb((db) => {
        db.collection("messages").find(messagesQuery).toArray(callback)
    })
}

var uncheckRequest = (companyId, branchId, gigaId, messageId, callback) => {
    let uncheckQuery = {
        "companyId": parseInt(companyId),
        "branchId": parseInt(branchId),
        "gigaId": parseInt(gigaId),
        "messageId": messageId
    }

    getDb((db) => {
        db.collection("messages").update(uncheckQuery, {
            $unset: { 
                 "isApproved": "",
                 "decisionDescription": ""
            }
        }, callback)
    })
}

var requestDecision = (username, companyId, branchId, branchPhoneNumber, gigaId, gigaName, messageId, decisionDescription, isApproved, callback) => {
    let decisionQuery = {
        "companyId": parseInt(companyId),
        "branchId": parseInt(branchId),
        "gigaId": parseInt(gigaId),
        "messageId": messageId
    }

    getDb((db) => {
        db.collection("messages").update(decisionQuery, {
            $set: { 
                 "decisionDescription": decisionDescription,
                 "isApproved": isApproved
            }
        }, (err, ans) => {
            if(err) {
                callback(err, ans)
                return
            } else {
                if(fcmTokenByUsername[username]) {
                    fcmWrapper.notifyRequestAnswer(fcmTokenByUsername[username], gigaName, branchPhoneNumber, decisionDescription, isApproved, (err, response) => {
                        if(err) {
                            console.log('send broadcast notification error')
                            console.log(err)
                        }
                    })
                }

                callback(err, ans)
            }
        })
    })
}

var uploadRequest = (username, gigaMajor, gigaMinor, title, content, callback) => {
    let uploadedRequest = {
        "username": username,
        "companyId": gigaMajor,
        "branchId": parseInt(parseInt(gigaMinor) / 256),
        "gigaId": parseInt(gigaMinor) % 256,
        "messageId": crypto.randomBytes(32).toString('hex'),
        "title": title,
        "content": content,
        "uploadTime": new Date().getTime()
    }

    getDb((db) => {
        db.collection("messages").insertOne(uploadedRequest).then((resp) => {
            callback(resp.result.ok == 1)
        })
    })
}

module.exports = {
    "getRequests": getRequests,
    "requestDecision": requestDecision,
    "uncheckRequest": uncheckRequest,
    "uploadRequest": uploadRequest
}