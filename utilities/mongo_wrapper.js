var MongoClient = require('mongodb').MongoClient
var assert = require('assert')
var db;
var _ = require("lodash")
// Connection URL
var ip = process.env.DB || "52.58.9.60"

// NOT best practice but for now its ok. if someone asks, idan did it
var username = 'wheel'
var pass = 'Aa123456'
var dbName = 'wheel'
var url = `mongodb://${username}:${pass}@${ip}:27017/${dbName}?authSource=${username}`

module.exports = {
    getDb: (callback) => {
        if (db == undefined) {
            MongoClient.connect(url, function (err, localdb) {
                assert.equal(null, err)
                db = localdb
                console.log("Connected correctly to server")
                callback(db)
            })
        } else {
            callback(db)
        }
    }
}