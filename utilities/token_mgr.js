var firebase = require("firebase-admin");
var firebase_account = require("../demogiga-8c9c3-firebase-adminsdk-pqdv4-0be3d726e3.json");

var user_list = Array();

module.exports = {
    addToken : function (chain, branch, user, token) {
        
        for (var i = 0 ; i < user_list.length ; i++) {
            var object = user_list[i];
            if (chain == object.chain && branch == object.branch && user == object.user) {
                user_list[i].token = token;
            }
        }

        var object = new Object();
        object.chain = chain;
        object.branch = branch;
        object.user = user;
        object.token = token;
        user_list.push(object);
    },
    
    sendMessage : function (chain, branch, user, msg) {
        for (var i = 0 ; i < user_list.length ; i++){
            var object = user_list[i];
            if (chain == object.chain && branch == object.branch && user == object.user) {
                var token = object.token;
                firebase.messaging().sendToDevice(token, msg)
                .then((response)=> {
                    // See the MessagingDevicesResponse reference documentation for
                    // the contents of response.
                    console.log("Successfully sent message:", response);
                })
                .catch(function(error) {
                    console.log("Error sending message:", error);
                });
                return;
            }
        }

        return false;
    },

    removeToken: function (chain, branch, user) {
        for (var i = 0 ; i < user_list.length ; i++){
            var object = user_list[i];
            if (chain == object.chain && branch == object.branch && user == object.user) {
                user_list.splice(i, 1);
                return;
            }
        }
    }


}