var bodyParser = require('body-parser')
var express = require("express")
let clientProjectName = "webzonn"
module.exports = function (app) {
    // Request body parser
    app.use(bodyParser.json({limit: '5mb'}));
    app.use(bodyParser.urlencoded({
        limit: '5mb',
        extended: true
    }));

    // Response headers
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    // Define Routes
    require('./new_web_routes/requests')(app)
    require('./new_web_routes/branches')(app)
    
    app.use('/', express.static(__dirname + `/../${clientProjectName}/dist`))
    app.use('/scripts', express.static(__dirname + `/../${clientProjectName}/node_modules`))
    app.use('/pics', express.static(__dirname + `/../${clientProjectName}/client/pics`))
    app.use('/resources', express.static(__dirname + '/resources'))
}